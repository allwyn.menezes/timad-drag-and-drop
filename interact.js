 //interact js lib (https://interactjs.io/docs/)
 interact('.item')
 .draggable({
   inertia: true,
   modifiers: [
     interact.modifiers.restrictRect({
       restriction: document.querySelector('#dropzone'),
       endOnly: true,
     })
   ],
   
   onmove: function(event) {
   
     const target = event.target;
 
     const dataX = target.getAttribute('data-x');
     const dataY = target.getAttribute('data-y');
     const initialX = parseFloat(dataX) || 0;
     const initialY = parseFloat(dataY) || 0;
 
     const deltaX = event.dx;
     const deltaY = event.dy;
 
     const newX = initialX + deltaX;
     const newY = initialY + deltaY;
 
     target
       .style
       .transform = `translate(${newX}px, ${newY}px)`;
 
     target.setAttribute('data-x', newX);
     target.setAttribute('data-y', newY);
   }
 })
 interact('.dropzone')
 .dropzone({
   accept: '.item',
   overlap: 0.0,
   ondropactivate: function (event) {
     const item = event.relatedTarget
     item.classList.add('dragging')
   },
   ondropdeactivate: function (event) {
     const item = event.relatedTarget
     item.classList.remove('dragging', 'cannot-drop')
   },
   ondragenter: function(event) {
     const item = event.relatedTarget
     item.classList.remove('cannot-drop')
     item.classList.add('can-drop')
   },
   ondragleave: function(event) {
     const item = event.relatedTarget
     item.classList.remove('can-drop')
     item.classList.add('cannot-drop')
   },
   ondrop: function (event) {
       const item = event.relatedTarget
       console.log(item)
     },
 })
 
   interact('.resize-drag')
   .resizable({
     // resize from particualr edges and corners which are set to true
     edges: { left: true, right: true, bottom: false, top: false },
 
     listeners: {
       move (event) {
         var target = event.target
         var x = (parseFloat(target.getAttribute('data-x')) || 0)
         var y = (parseFloat(target.getAttribute('data-y')) || 0)
 
         // update the element's style
         target.style.width = event.rect.width + 'px'
         target.style.height = event.rect.height + 'px'
 
         // translate when resizing from top or left edges
         x += event.deltaRect.left
         y += event.deltaRect.top
 
         target.style.transform = 'translate(' + x + 'px,' + y + 'px)'
 
         target.setAttribute('data-x', x)
         target.setAttribute('data-y', y)
       } 
     },
     modifiers: [
       // keep the edges inside the parent
       interact.modifiers.restrictEdges({
         outer: 'parent'
       }),
 
       // minimum size
       interact.modifiers.restrictSize({
         min: { width: 100, height: 50 }
       })
     ],
 
     inertia: true
   })
 