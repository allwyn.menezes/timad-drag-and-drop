let tableCount = 0;
    function onDragStart(event) {
      event.dataTransfer.setData('text/html', event.target.id);
      event.currentTarget.style.backgroundColor = 'yellow';
    }
    function onDragOver(event) {
      event.preventDefault();
    }
    function onDrop(event) {
        
      
      $('#dropzone').children('h2').remove();
      let index = 0;
      const id = event.dataTransfer.getData('text/html');
      const draggableElement = document.getElementById(id);
      const draggedElementInnerHtml =  draggableElement.innerHTML
      //console.log
      let element = ""

      //here title code is generated
      if(draggedElementInnerHtml === "Title"){
        let divForTitle  = document.createElement('div')
        let title = document.createElement('H2');
        titleText = document.createTextNode("Your Title")
        title.appendChild(titleText)
        title.setAttribute('contentEditable', true)
        title.setAttribute('class', 'h2title')
        
        divForTitle.appendChild(title)
        divForTitle.setAttribute('style', 'margin:10px')
        divForTitle.setAttribute('id', 'divForTitle')
        divForTitle.setAttribute('class', 'item')

        element = divForTitle

      }else if(draggedElementInnerHtml === "Label"){
        let divForLabel = document.createElement("div")
        let newLabel = document.createElement(draggedElementInnerHtml);
        newLabel.setAttribute("for", 'input');
        newLabel.setAttribute('contentEditable', true)
        
        newLabel.innerHTML = "Your Label";
        divForLabel.appendChild(newLabel)
        divForLabel.setAttribute('style', 'margin:10px')
        newLabel.setAttribute('style', 'margin:0px 5px 0px 0px')
        divForLabel.setAttribute('id', 'divForLabel')

        divForLabel.setAttribute('class', 'item')

        element = divForLabel

      }else if (draggedElementInnerHtml === "Input"){
        element = document.createElement(draggedElementInnerHtml);
        element.type = "text";
        element.id = "input_"+index 
        element.setAttribute('value', '');
        element.setAttribute('class', 'item')
        index++

      }else if (draggedElementInnerHtml === "Button"){
        element = document.createElement(draggedElementInnerHtml);
        element.type = "button";
        element.textContent = "Button"
        element.setAttribute('id', 'button')
        element.setAttribute('contentEditable', true)
        element.setAttribute('class', 'item')

      }else if(draggedElementInnerHtml === "Table"){
        tableCount++
        let divForTable = document.createElement("div")


        divForTable.setAttribute('id', "divTable_"+tableCount)
        divForTable.setAttribute('class', 'item')
        

        let myTableDiv = document.getElementById("mtable");

        let addRowBtn = document.createElement('button')
        addRowBtn.textContent = "Add Row"
        addRowBtn.setAttribute('onClick', "addRow()")
        addRowBtn.setAttribute('class', 'button addRowBtn')
        divForTable.appendChild(addRowBtn)

        let addColumnBtn = document.createElement('button')
        addColumnBtn.textContent = "Add Column"
        addColumnBtn.setAttribute('onClick', "addColumn()")
        addColumnBtn.setAttribute('class', 'button addColumnBtn')
        divForTable.appendChild(addColumnBtn)

        let removeTableBtn = document.createElement('button')
        removeTableBtn.textContent = "Remove Table"
        removeTableBtn.setAttribute('id', "removeBtn_"+tableCount)
        removeTableBtn.setAttribute('onClick', "removeTable(this)")
        removeTableBtn.setAttribute('class', 'button')
        divForTable.appendChild(removeTableBtn)
        
        let spanForTableHeading = document.createElement('span')
        let tableHeading = document.createElement('H3');
        spanForTableHeading.appendChild(tableHeading)
        
        tableHeadingtext = document.createTextNode("Table Heading")
        tableHeading.append(tableHeadingtext)
        tableHeading.setAttribute('contentEditable', true)
        divForTable.appendChild(spanForTableHeading);
        tableHeading.setAttribute('class', 'h2title')
    
        let table = document.createElement('TABLE');
        table.border='1';

        let tablehead = document.createElement('THEAD');
        table.appendChild(tablehead);

        let tr = document.createElement('TR');
        tablehead.appendChild(tr);
          
        for (let k=0; k<4; k++){
            let th = document.createElement('TH');
            th.width='100';
            th.height = '30';
            th.setAttribute('contentEditable', true)
            //let inputForThead = document.createElement('label')
            //inputForThead.setAttribute('contentEditable', true)
            // inputForThead.setAttribute('value', '')
            //th.appendChild(inputForThead);
            tr.appendChild(th);
        }

        let tableBody = document.createElement('TBODY');
        tableBody.id = "myTableBody"
        table.appendChild(tableBody);

        let trForBody = document.createElement('TR');
        tableBody.appendChild(trForBody);
        
        for (let l=0; l<4; l++){
          let td = document.createElement('TD');
          td.width='100';
          let inputForTbody = document.createElement('input')
          inputForTbody.setAttribute('value', '')
          td.appendChild(inputForTbody);
          trForBody.appendChild(td);
        }

        table.setAttribute('style', 'margin:20px 0px 20px 10px')
        table.setAttribute('class', 'resize-drag')
        divForTable.appendChild(myTableDiv.appendChild(table));

        element = divForTable
      }
      
      const dropzone = event.target;
      // let clone = draggableElement.cloneNode(true)

      dropzone.appendChild(element);
      event.dataTransfer.clearData();
    }