function addColumn(){
    let removeRBtn = document.createElement('Button')
    removeRBtn.textContent = 'Remove Row'
    removeRBtn.setAttribute('onclick', 'deleteRow(this)')

    $("tr:first").append("<th contenteditable='true'></th>");
    $("tr:not(:first)").append("<td><input type='text' value=''></td>");
    $("tr").find('button').remove()
    $("tr:not(:first)").append(removeRBtn)
  };
  function addRow(){
    let colsCount = $("table > tbody > tr:first > td").length;
    let newTr = document.createElement('TR');

    let removeRowBtn = document.createElement('Button')
    removeRowBtn.textContent = 'Remove Row'
    removeRowBtn.setAttribute('onclick', 'deleteRow(this)')

    for (let m=0; m<colsCount; m++){
      let tds = document.createElement('TD');
      tds.width='100';
      let inputForTableBodyNewCreated = document.createElement('input')
      inputForTableBodyNewCreated.setAttribute('value', '')
      tds.appendChild(inputForTableBodyNewCreated);
      newTr.appendChild(tds);
    }
    let lastRow = document.getElementById("myTableBody"); 
    newTr.appendChild(removeRowBtn)
    lastRow.appendChild(newTr)
  }
  function deleteRow(el) {

    // while there are parents, keep going until reach TR 
    while (el.parentNode && el.tagName.toLowerCase() != 'tr') {
       el = el.parentNode;
    }
  
    // If el has a parentNode it must be a TR, so delete it
    // Don't delte if only 1 rows left in table
  if (el.parentNode && el.parentNode.rows.length > 1) {
     el.parentNode.removeChild(el);
  }
  }
   //Remove Table on button click /icon  click form dropzone
   function removeTable(t) {
    console.log(document.getElementById(t.parentElement.id))
    let removeTab = document.getElementById(t.parentElement.id).remove()
  }